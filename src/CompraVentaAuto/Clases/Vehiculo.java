
package CompraVentaAuto.Clases;

import CompraVentaAuto.Herramienta.ConvertFileToClass;
import CompraVentaAuto.Herramienta.ProcesadorArchivo;
import java.io.File;
import java.util.ArrayList;


public class Vehiculo implements ConvertFileToClass{
    
    private final String TIPO_AUTO = "AUTO";
    private final String TIPO_CAMIONETA = "CAMIONETA";
    private final String TIPO_MOTO = "MOTO";
    private final String TIPO_CAMION = "CAMION";
    
    private final String ARCHIVO = "VEHICULO.CSV";
    private String _directorio=null;
    
    private String _tipo;
    private String _placa;
    private String _marca;
    private String _modelo;
    private String _tipoMotor;
    private int _anio;
    private int _recorrido;
    private String _color;
    private String _tipoCombustible;
    private String _vidrios;
    private String _transmision;
    private double _precio;
    
    private ProcesadorArchivo _procesadorFile;
    
    
    public Vehiculo()
    {
        this._directorio = new File("").getAbsolutePath()+ "\\ARCHIVOS";
        _procesadorFile = new ProcesadorArchivo(this._directorio, ARCHIVO, this);
    }
    
    public static String ExisteVehiculo(ArrayList vehiculos,Vehiculo buscada)
    {
        String msgOut = null;
        
        if(vehiculos!=null)
        {
            for(int i=0;i<vehiculos.size();i++)
            {
                Vehiculo veh = (Vehiculo)vehiculos.get(i);
                if(veh.getPlaca().equalsIgnoreCase(buscada.getPlaca()))
                {
                    msgOut = "El vehículo ya se encuentra registrado.";
                    break;
                }
            }
        }
        
        return msgOut;
    }

    @Override
    public ArrayList GetArrayListVacioInstancia() {
        return new ArrayList<Vehiculo>();
    }

    @Override
    public Object ConvertLineaToClase(String lineaArchivo) {
        Vehiculo veh = null;
        if(lineaArchivo!=null && !lineaArchivo.trim().equals(""))
        {
            veh = new Vehiculo();
            String[] datos = lineaArchivo.split(";");
            veh._tipo = datos[0];
            veh._placa = datos[1];
            veh._marca = datos[2];
            veh._modelo = datos[3];
            veh._tipoMotor = datos[4];
            veh._anio = Integer.parseInt(datos[5]);
            veh._recorrido = Integer.parseInt(datos[6]);
            veh._color = datos[7];
            veh._tipoCombustible = datos[8];
            veh._vidrios = datos[9];
            veh._transmision = datos[10];
            veh._precio = Double.parseDouble(datos[11]);
            
        }
        return veh;
    }
    
    @Override
    public String Registrar()
    {
        String msgOut = null;
        try
        {
            String registro= this._tipo + ";" + this._placa + ";" + this._marca + ";" + this._modelo + ";" +
                            this._tipoMotor + ";" + this._anio + ";" + this._recorrido + ";" + 
                            this._color + ";" + this._tipoCombustible + ";" + this._vidrios + ";" + 
                            this._transmision  + ";" + this._precio;
            
            msgOut = this._procesadorFile.EscribirLinea(registro);
        }
        catch(Exception ex)
        {
            msgOut = "Error: No se pudo agregar el nuevo vehiculo inténtelo de nuevo";
        }
                
        return msgOut;
    }

    @Override
    public ArrayList GetListArchivoDBClase() {
        ArrayList<Vehiculo> lista = null;
        lista = _procesadorFile.GetListadoClases();
        if(lista==null)
            lista = new ArrayList<>();
        return lista;
    }
    
    public static ArrayList<Vehiculo> GetVehiculosxCriterios(ArrayList<Vehiculo> vehiculos,String tipoVehiculo, int recorridoI, int recorridoF,
                                                int anioI, int anioF, double precioI, double precioF)
    {
        ArrayList<Vehiculo> vehiculosBusq = new ArrayList<>();
        
        for(int i=0;i<vehiculos.size();i++)
        {
            boolean incluir =true;
            Vehiculo v = vehiculos.get(i);
           
            incluir = incluir && v.getTipo().equalsIgnoreCase(tipoVehiculo);
            
            if(recorridoI>0)
            {
                incluir = incluir && v.getRecorrido()>=recorridoI;
            }
            if(recorridoF>0)
            {
                incluir = incluir && v.getRecorrido()<=recorridoF;
            }
            if(anioI>0)
            {
                incluir = incluir && v.getAnio()>=anioI;
            }
            if(anioF>0)
            {
                incluir = incluir && v.getAnio()<=anioF;
            }
            if(precioI>0)
            {
                incluir = incluir && v.getPrecio()>=precioI;   
            }
            if(precioF>0)
            {
                incluir = incluir && v.getPrecio()<=precioF;
            }
                
            if(incluir)
                vehiculosBusq.add(v);
        }
        
        return vehiculosBusq;
    }
    
    public String getTipo() {
        return _tipo;
    }

    public void setTipo(String tipo) {
        this._tipo = tipo;
    }

    public String getPlaca() {
        return _placa;
    }

    public void setPlaca(String _placa) {
        this._placa = _placa;
    }

    public String getMarca() {
        return _marca;
    }

    public void setMarca(String _marca) {
        this._marca = _marca;
    }

    public String getModelo() {
        return _modelo;
    }

    public void setModelo(String _modelo) {
        this._modelo = _modelo;
    }

    public String getTipoMotor() {
        return _tipoMotor;
    }

    public void setTipoMotor(String _tipoMotor) {
        this._tipoMotor = _tipoMotor;
    }

    public int getAnio() {
        return _anio;
    }

    public void setAnio(int _anio) {
        this._anio = _anio;
    }

    public int getRecorrido() {
        return _recorrido;
    }

    public void setRecorrido(int _recorrido) {
        this._recorrido = _recorrido;
    }

    public String getColor() {
        return _color;
    }

    public void setColor(String _color) {
        this._color = _color;
    }

    public String getTipoCombustible() {
        return _tipoCombustible;
    }

    public void setTipoCombustible(String _tipoCombustible) {
        this._tipoCombustible = _tipoCombustible;
    }

    public String getVidrios() {
        return _vidrios;
    }

    public void setVidrios(String _vidrios) {
        this._vidrios = _vidrios;
    }

    public String getTransmision() {
        return _transmision;
    }

    public void setTransmision(String _transmision) {
        this._transmision = _transmision;
    }

    public double getPrecio() {
        return _precio;
    }

    public void setPrecio(double _precio) {
        this._precio = _precio;
    }
    

}
